# Notification using chatter private message

## Description.

This is a system of notification about important object events.

For objects Opportunity, Account, Contact and Contracts have created lookup link to the object SalesTeam. SalesTeam holds an information about users which want to receive messages about events and what type of message they want to receive.

Users should receive chatter private messages when event triggered.

Event types:

   *  Object created
   *  Owner changed
   *  Status changed (If object do not have status field it needs to be created)

Each event type has unique message template.

## Installation

1. Create a scratch org and provide it with an alias (**notify** in the command below):

    ```
    sfdx force:org:create -s -f config/project-scratch-def.json -a notify
    ```

1. Push the app to your scratch org:
   
    ```
    sfdx force:source:push
    ```
   
1. Assign the **Sales Team** permission set to the default user:

    ```
    sfdx force:user:permset:assign -n Sales_Team
    ```

## Resource

- [Salesforce Extensions Documentation](https://developer.salesforce.com/tools/vscode/)
- [Salesforce CLI Setup Guide](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_intro.htm)
- [Salesforce DX Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_intro.htm)
- [Salesforce CLI Command Reference](https://developer.salesforce.com/docs/atlas.en-us.sfdx_cli_reference.meta/sfdx_cli_reference/cli_reference.htm)
