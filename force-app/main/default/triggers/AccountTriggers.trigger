/**
 * Created by Serg on 22.05.2021.
 */

trigger AccountTriggers on Account (after insert, after update) {

    AccountTriggerHandlers.triggerHandler((List<Account>)Trigger.new, (Map<Id, Account>)Trigger.oldMap, Trigger.operationType);
}