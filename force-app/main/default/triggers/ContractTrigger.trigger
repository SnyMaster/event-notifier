/**
 * Created by Serg on 24.05.2021.
 */

trigger ContractTrigger on Contract (after insert, after update) {

    ContractTriggerHandlers.triggerHandler((List<Contract>)Trigger.new, (Map<Id, Contract>)Trigger.oldMap, Trigger.operationType);
}