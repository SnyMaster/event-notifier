/**
 * Created by Serg on 24.05.2021.
 */

trigger OpportubityTrigger on Opportunity (after insert, after update) {

    OpportunityTriggerHandlers.triggerHandler((List<Opportunity>)Trigger.new, (Map<Id, Opportunity>)Trigger.oldMap, Trigger.operationType);
}