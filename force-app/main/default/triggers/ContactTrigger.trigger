/**
 * Created by Serg on 24.05.2021.
 */

trigger ContactTrigger on Contact (after insert, after update) {

    ContactTriggerHandlers.triggerHandler((List<Contact>)Trigger.new, (Map<Id, Contact>)Trigger.oldMap, Trigger.operationType);
}