/**
 * Created by Serg on 24.05.2021.
 */

@IsTest
private class NotificationMessageTest {
    @TestSetup
    static void initData() {
        TestDataFactory.initUsers();
        Test.startTest();
        TestDataFactory.initContact();
        Test.stopTest();
    }
    @IsTest
    static void testBehavior() {
        TestDataFactory.initAccount();
        Account acc = TestDataFactory.getAccount();
        String template = '{objectId} {objectName}';
        NotificationMessage notificationMessage = new NotificationMessage(template);
        System.assert(notificationMessage.getMessage(acc).equals(acc.Id + ' ' + acc.Name));
        System.assert(notificationMessage.getMessage(null).equals(template));
    }
}