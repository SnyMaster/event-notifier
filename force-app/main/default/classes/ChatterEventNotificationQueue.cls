/**
 * Created by Serg on 22.05.2021.
 */

public class ChatterEventNotificationQueue implements Queueable {
    private List<SObject> listObjects;
    private NotificationMessageInterface NotificationMessage;
    private ChatterServiceInterface chatService;

    public ChatterEventNotificationQueue(
            List<SObject> listObjects,
            NotificationMessageInterface notificationMessage,
            ChatterServiceInterface chatService) {

        this.listObjects = listObjects;
        this.NotificationMessage = notificationMessage;
        this.chatService = chatService;
    }

    public void execute(QueueableContext context) {
        if (this.listObjects.isEmpty()) {
            return;
        }
        SObject objectItem  = this.listObjects.remove(0);
        this.chatService.send(this.NotificationMessage.getMessage(objectItem));
        if (this.listObjects.isEmpty() || Test.isRunningTest()) {
            return;
        }
        System.enqueueJob(new ChatterEventNotificationQueue(this.listObjects,this.NotificationMessage,this.chatService));
    }
}