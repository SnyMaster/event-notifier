/**
 * Created by Serg on 22.05.2021.
 */

public class AccountTriggerHandlers {
    public static Boolean bypassTrigger = false;
    public static void triggerHandler(List<Account> newAccounts, Map<Id, Account> oldAccountsMap, System.TriggerOperation triggerEvent) {

        if (bypassTrigger) {
            return;
        }
        SalesTeamEventHandler.triggerHandler(newAccounts, oldAccountsMap, triggerEvent);
    }
}    
