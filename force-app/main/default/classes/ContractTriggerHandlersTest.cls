/**
 * Created by Serg on 24.05.2021.
 */

@IsTest
private class ContractTriggerHandlersTest {
    @TestSetup
    static void initData() {
        TestDataFactory.initUsers();
        Test.startTest();
        TestDataFactory.initContract();
        Test.stopTest();
    }

    @IsTest
    static void testInsert() {
        List<FeedItem> feedItems = [SELECT Id, ParentId FROM FeedItem];
        System.assertEquals(2, feedItems.size());
        System.assertEquals(1, [SELECT Id FROM USER WHERE id = :feedItems[0].ParentId].size());
    }

    @IsTest
    static void testUpdate() {
        List<FeedItem> feedItems = [SELECT Id, ParentId FROM FeedItem];
        System.assertEquals(2, feedItems.size());
        Contract cntr = TestDataFactory.getContract();
        User usr = [SELECT Id FROM User WHERE id != :feedItems[0].ParentId LIMIT 1];
        cntr.OwnerId = usr.Id;
        Test.startTest();
        update cntr;
        Test.stopTest();
        feedItems = [SELECT Id, ParentId FROM FeedItem];
        System.assertEquals(3, feedItems.size());
    }
}