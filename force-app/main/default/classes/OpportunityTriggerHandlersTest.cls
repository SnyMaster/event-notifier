/**
 * Created by Serg on 24.05.2021.
 */

@IsTest
private class OpportunityTriggerHandlersTest {
    @TestSetup
    static void initData() {
        TestDataFactory.initUsers();
        Test.startTest();
        TestDataFactory.initOpportunity();
        Test.stopTest();
    }

    @IsTest
    static void testInsert() {
        List<FeedItem> feedItems = [SELECT Id, ParentId FROM FeedItem];
        System.assertEquals(2, feedItems.size());
        System.assertEquals(1, [SELECT Id FROM USER WHERE id = :feedItems[0].ParentId].size());
    }

    @IsTest
    static void testUpdate() {
        List<FeedItem> feedItems = [SELECT Id, ParentId FROM FeedItem];
        System.assertEquals(2, feedItems.size());
        Opportunity opp = TestDataFactory.getOpportunity();
        User usr = [SELECT Id FROM User WHERE id != :feedItems[0].ParentId LIMIT 1];
        opp.OwnerId = usr.Id;
        Test.startTest();
        update opp;
        Test.stopTest();
        feedItems = [SELECT Id, ParentId FROM FeedItem];
        System.assertEquals(3, feedItems.size());
    }

}