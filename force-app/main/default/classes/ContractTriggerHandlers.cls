/**
 * Created by Serg on 24.05.2021.
 */

public with sharing class ContractTriggerHandlers {

    public static void triggerHandler(List<Contract> newContracts, Map<Id, Contract> oldContractsMap, System.TriggerOperation triggerEvent) {

        SalesTeamEventHandler.triggerHandler(newContracts, oldContractsMap, triggerEvent);
    }
}