/**
 * Created by Serg on 23.05.2021.
 */

public class SalesTeamEventHandler {
    
    public static void triggerHandler(List<SObject> newObjects, Map<Id, Sobject> oldObjectsMap, System.TriggerOperation triggerEvent) {
        switch on triggerEvent {

            when AFTER_INSERT {
                afterInsert(newObjects);
            }
            when AFTER_UPDATE {
                afterUpdate(newObjects, oldObjectsMap, new Set<String>{'Status__c', 'Status'}, SalesTeamService.TYPE_UPDATED_STATUS);
                afterUpdate(newObjects, oldObjectsMap, new Set<String>{'OwnerId'}, SalesTeamService.TYPE_UPDATED_OWNER);
            }
        }
    }

    private static void afterInsert(List<SObject> newObjects) {
        Set<Id> salesTeamIds = new Set<Id>();
        List<SObject> objectsInserted = new List<SObject>();
        for(SObject obj: newObjects) {
            salesTeamIds.add((Id)obj.get('Sales_Team__c'));
            objectsInserted.add(obj);
        }
        if (salesTeamIds.isEmpty()) {
            return;
        }
        executeJob(objectsInserted, SalesTeamService.TYPE_INSERTED, salesTeamIds);
    }

    private static void afterUpdate(List<SObject> newObjects, Map<Id, SObject> oldObjects, Set<String> checkUpdatedField, String eventType) {
        Set<Id> salesTeamIds = new Set<Id>();
        List<SObject> updatedObjects = new List<SObject>();
        for(SObject obj: newObjects) {
            if (isUpdated(checkUpdatedField, obj, oldObjects.get(obj.Id))) {
                updatedObjects.add(obj);
                salesTeamIds.add((Id)obj.get('Sales_Team__c'));
            }
        }
        if (salesTeamIds.isEmpty()) {
            return;
        }
        executeJob(updatedObjects, eventType, salesTeamIds);
    }

    private static void executeJob(List<SObject> listObjects, String eventType, Set<Id> salesTeamIds) {
        Set<Id> userIds = SalesTeamService.getUserIds(eventType, salesTeamIds);
        if (userIds.isEmpty()) {
            return;
        }
        eventType = Test.isRunningTest() ? 'test' : eventType;
        CustomMetadataHelper.EventNotificationTemplate template = new CustomMetadataHelper.EventNotificationTemplate(eventType);
        if (template.getEventNotificationTemplate() == null) {
            return;
        }
        try {
            NotificationMessageInterface notificationMessage = new NotificationMessage(template.getEventNotificationTemplate());
            System.enqueueJob(
                    new ChatterEventNotificationQueue(
                            listObjects,
                            notificationMessage,
                            new ChatterService(userIds).isRichText(true)
                    ));
        } catch (Exception ex) {}
    }

    private static Boolean isUpdated(Set<String> checkUpdatedField, SObject newObject, SObject oldObject) {
        for(String fieldName: checkUpdatedField) {
            if(newObject.getSobjectType().getDescribe().fields.getMap().containsKey(fieldName) &&
                    !newObject.get(fieldName).equals(oldObject.get(fieldName))) {
                return true;
            }
        }
        return false;
    }
}