/**
 * Created by Serg on 24.05.2021.
 */

@IsTest
private class CustomMetadataHelperTest {
    @IsTest
    static void testBehavior() {
        CustomMetadataHelper.EventNotificationTemplate eventNotificationTemplate = new CustomMetadataHelper.EventNotificationTemplate('testTest');
        eventNotificationTemplate = new CustomMetadataHelper.EventNotificationTemplate('test');
        System.assert(eventNotificationTemplate.getEventNotificationTemplate().equals('test'));
    }
}