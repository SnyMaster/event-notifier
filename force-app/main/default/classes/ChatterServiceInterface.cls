public interface ChatterServiceInterface {
    void send(String message);
}