/**
 * Created by Serg on 24.05.2021.
 */

@IsTest
public class TestDataFactory {

    public static void initUsers() {
        String uniqueUserName = 'user' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userTest1 = new User(Alias = 'standt1', Email='standarduser@testorg.com',
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = p.Id,
                TimeZoneSidKey='America/Los_Angeles',
                UserName=uniqueUserName);
        uniqueUserName = 'user' + DateTime.now().getTime() + '@testorg.com';
        User userTest2 = new User(Alias = 'stand2', Email='standarduser@testorg.com',
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = p.Id,
                TimeZoneSidKey='America/Los_Angeles',
                UserName=uniqueUserName);
        insert new List<User>{userTest1, userTest2};
        Sales_Team__c salesTeam = new Sales_Team__c(Name = 'Test Sales Team');
        insert  salesTeam;
        Sales_Team_User__c salesTeamUser = new Sales_Team_User__c(
                Sales_Team__c = salesTeam.Id,
                User__c = userTest1.Id,
                Interested_Notification_Types__c =
                        String.join(new List<String>{
                                SalesTeamService.TYPE_INSERTED,
                                SalesTeamService.TYPE_UPDATED_OWNER,
                                SalesTeamService.TYPE_UPDATED_STATUS
                        }, ';')
        );
        insert salesTeamUser;
    }

    public static Account initAccount() {
        Account acc = new Account(Name = 'Test account');
        Sales_Team__c salesTeam = getSalesTeam();
        if (salesTeam != null) {
            acc.Sales_Team__c = salesTeam.Id;
        }
        insert acc;
        return acc;
    }

    public static void initContact() {
        Contact cnt = new Contact(LastName = 'Test Contact');
        Sales_Team__c salesTeam = getSalesTeam();
        if (salesTeam != null) {
            cnt.Sales_Team__c = salesTeam.Id;
        }
        insert cnt;
    }

    public static void initContract() {
        Id IdAccount = initAccount().Id;
        Contract cntr = new Contract(AccountId = IdAccount);
        Sales_Team__c salesTeam = getSalesTeam();
        if (salesTeam != null) {
            cntr.Sales_Team__c = salesTeam.Id;
        }
        insert cntr;
    }

    public static void initOpportunity() {
        Id IdAccount = initAccount().Id;
        List<PicklistEntry> picklistEntries = PicklistUtils.getPicklistValues(
                Opportunity.SObjectType,
                Opportunity.StageName
        );
        Opportunity opp = new Opportunity(
                AccountId = IdAccount,
                Name = 'test',
                StageName = picklistEntries[0].value,
                CloseDate = Date.today().addDays(10)
        );
        Sales_Team__c salesTeam = getSalesTeam();
        if (salesTeam != null) {
            opp.Sales_Team__c = salesTeam.Id;
        }
        insert opp;
    }

    public static Sales_Team__c getSalesTeam() {
        return [SELECT Name FROM Sales_Team__c LIMIT 1];
    }

    public static Account getAccount() {
        return [SELECT Name, Sales_Team__c, Status__c, OwnerId FROM Account LIMIT 1];
    }

    public static Contact getContact() {
        return [SELECT LastName, Sales_Team__c, Status__c, OwnerId FROM Contact LIMIT 1];
    }

    public static  Contract getContract() {
        return [SELECT Name, Sales_Team__c, Status, OwnerId FROM Contract LIMIT 1];
    }

    public static  Opportunity getOpportunity() {
        return [SELECT Name, Sales_Team__c, Status__c, OwnerId FROM Opportunity LIMIT 1];
    }

    public static List<User> getSalesTeamUser() {
        Sales_Team__c salesTeam = getSalesTeam();
        return [SELECT Id FROM USER WHERE Id IN (SELECT User__c FROM Sales_Team_User__c WHERE Sales_Team__c = :salesTeam.Id)];
    }
}