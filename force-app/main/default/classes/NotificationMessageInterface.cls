/**
 * Created by Serg on 24.05.2021.
 */

public interface NotificationMessageInterface {
    String getMessage(SObject contentObject);
}