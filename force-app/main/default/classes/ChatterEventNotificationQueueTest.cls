/**
 * Created by Serg on 24.05.2021.
 */

@IsTest
private class ChatterEventNotificationQueueTest {
    @IsTest
    static void testBehavior() {
        AccountTriggerHandlers.bypassTrigger = true;
        TestDataFactory.initUsers();
        TestDataFactory.initAccount();
        TestDataFactory.initAccount();
        List<Account> accounts = new List<Account>();
        Notification notificationMsg = new Notification();
        Set<Id> userIds = new Set<Id>();
        Test.startTest();
        System.enqueueJob(
                new ChatterEventNotificationQueue(
                        accounts,
                        notificationMsg,
                        new ChatService(userIds)
                ));
        for( User usr: TestDataFactory.getSalesTeamUser()) {
            userIds.add(usr.Id);
        }
        System.enqueueJob(
                new ChatterEventNotificationQueue(
                        accounts,
                        notificationMsg,
                        new ChatService(userIds)
                ));
        accounts = [SELECT Id, Sales_Team__c FROM Account];
        System.enqueueJob(
                new ChatterEventNotificationQueue(
                        accounts,
                        notificationMsg,
                        new ChatService(userIds)
                ));
        Test.stopTest();
        System.assertEquals(1, [SELECT Id FROM FeedItem].size());
    }

    private class Notification implements NotificationMessageInterface {
        public String getMessage(SObject contextObject) {
            return 'test';
        }
    }

    private class ChatService implements ChatterServiceInterface{

        private Set<Id> recipientsIds;
        private Boolean isRichText = false;

        public ChatService(Set<Id> recipientsIds) {
            this.recipientsIds = recipientsIds;
        }

        public void send(String message) {
            List<FeedItem> items = new List<FeedItem>();
            for(Id recipientId: this.recipientsIds) {
                FeedItem item = new FeedItem();
                item.Body = message;
                item.ParentId = recipientId;
                items.add(item);
            }
            insert items;
            return;
        }
    }
}