/**
 * Created by Serg on 23.05.2021.
 */

@IsTest
private class SalesTeamServiceTest {
    @TestSetup
    static void initData() {
        TestDataFactory.initUsers();
    }

    @IsTest
    static void testGetUsers() {
        Id salesTeamId = [SELECT Id FROM Sales_Team__c LIMIT 1].Id;
        System.assertEquals(1, SalesTeamService.getUserIds(SalesTeamService.TYPE_INSERTED, new Set<Id>{salesTeamId}).size());
        System.assertEquals(1, SalesTeamService.getUserIds(SalesTeamService.TYPE_UPDATED_OWNER, new Set<Id>{salesTeamId}).size());
        System.assertEquals(1, SalesTeamService.getUserIds(SalesTeamService.TYPE_UPDATED_STATUS, new Set<Id>{salesTeamId}).size());
        System.assertEquals(0, SalesTeamService.getUserIds(SalesTeamService.TYPE_INSERTED, new Set<Id>{'000000000000AAA'}).size());
    }

    @IsTest
    static void testExceptionBehavior() {
        try {
            SalesTeamService.getUserIds(null, null);
        }
        catch (Exception e) {}
    }

}