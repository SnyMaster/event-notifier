/**
 * Created by Serg on 22.05.2021.
 */

public class ChatterService implements ChatterServiceInterface{

    private Set<Id> recipientsIds;
    private Boolean isRichText = false;

    public ChatterService(Set<Id> recipientsIds) {
        this.recipientsIds = recipientsIds;
    }

    public ChatterService isRichText(Boolean isRichText) {
        this.isRichText = isRichText;
        return this;
    }

    public void send(String message) {
        if (this.recipientsIds.isEmpty()) {
            return;
        }
        List<FeedItem> items = new List<FeedItem>();
        for(Id recipientId: this.recipientsIds) {
            FeedItem item = new FeedItem();
            item.Body = message;
            item.ParentId = recipientId;
            item.IsRichText = this.isRichText;
            items.add(item);
        }
        insert items;
        return;
    }
}