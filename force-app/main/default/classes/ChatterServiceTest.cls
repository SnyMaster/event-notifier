/**
 * Created by Serg on 25.05.2021.
 */

@IsTest
private class ChatterServiceTest {
    @IsTest
    static void testBehavior() {
        TestDataFactory.initUsers();
        Set<Id> userIds = new Set<Id>();
        ChatterService chatService = new ChatterService(userIds);
        chatService.isRichText(true);
        chatService.send('test');
        System.assert([SELECT Id FROM FeedItem].isEmpty());
        for( User usr: TestDataFactory.getSalesTeamUser()) {
            userIds.add(usr.Id);
        }
        chatService = new ChatterService(userIds);
        chatService.send('test');
        System.assertEquals(1, [SELECT Id FROM FeedItem].size());
    }
}