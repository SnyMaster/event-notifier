/**
 * Created by Serg on 23.05.2021.
 */

public class CustomMetadataHelper {

    public class EventNotificationTemplate {
        private Event_Notification_Template__mdt eventNotificationTemplate;

        public EventNotificationTemplate(String eventType) {
            this.eventNotificationTemplate = getEventNotificationItem(eventType);
        }
        public String getEventNotificationTemplate() {
            return this.eventNotificationTemplate?.Template__c;
        }

        private Event_Notification_Template__mdt getEventNotificationItem(String eventType) {
            List<Event_Notification_Template__mdt> listEvent = [
                    SELECT Template__c, DeveloperName
                    FROM Event_Notification_Template__mdt
                    WHERE Is_Active__c = true AND DeveloperName = :eventType
            ];
            System.debug(listEvent);
            return listEvent.size() == 0 ? null : listEvent[0];
        }
    }
}