/**
 * Created by Serg on 24.05.2021.
 */

public with sharing class ContactTriggerHandlers {

    public static void triggerHandler(List<Contact> newContacts, Map<Id, Contact> oldContactsMap, System.TriggerOperation triggerEvent) {

        SalesTeamEventHandler.triggerHandler(newContacts, oldContactsMap, triggerEvent);
    }    
}