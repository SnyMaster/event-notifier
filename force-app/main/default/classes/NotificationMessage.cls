/**
 * Created by Serg on 22.05.2021.
 */

public class NotificationMessage implements NotificationMessageInterface {
    private String template;

    public NotificationMessage(String template) {
        this.template = template;

    }
    public String getMessage(SObject contextObject) {
        String message = this.template;
        Map<String, Object> templateValues = this.getValues(contextObject);
        for (String param : templateValues.keySet()) {
            message = message.replace(param, (String) templateValues.get(param));
        }
        return message;
    }

    private List<String> getTemplateObjectFieldList() {
        List<String> fields = new List<String>();
        Pattern pp = Pattern.compile('\\{object(\\w+)\\}');
        Matcher matcherResult = pp.matcher(this.template);
        while (matcherResult.find()) {
            fields.add(matcherResult.group(1));
        }
        return fields;
    }

    private Map<String, Object> getValues(SObject contextObject) {

        Map<String, Object> templateValues = new Map<String, Object>();
        if (contextObject == null) {
            return templateValues;
        }
        templateValues.put('{baseUrl}', System.Url.getSalesforceBaseURL().toExternalForm());
        templateValues.put('{objectName}', contextObject.getSObjectType().getDescribe().name);
        Map<String, Object> fieldsToValue = contextObject .getPopulatedFieldsAsMap();
        for(String fieldName: this.getTemplateObjectFieldList()) {
            if (!fieldsToValue.containsKey(fieldName)) {
                continue;
            }
            templateValues.put('{object' + fieldName + '}', fieldsToValue.get(fieldName));
        }
        return templateValues;
    }
}