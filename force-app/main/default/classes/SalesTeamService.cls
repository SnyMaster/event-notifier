/**
 * Created by Serg on 22.05.2021.
 */

public with sharing class SalesTeamService {
    public static String TYPE_INSERTED = 'inserted';
    public static String TYPE_UPDATED_OWNER = 'updatedOwner';
    public static String TYPE_UPDATED_STATUS = 'updatedStatus';

    public static Set<Id> getUserIds(String notificationType, Set<Id> salesTeamId) {
        return (new Map<Id, User>([
                    SELECT Id FROM USER WHERE Id IN (
                        SELECT User__c
                        FROM Sales_Team_User__c
                        WHERE
                            Interested_Notification_Types__c INCLUDES (:notificationType) AND
                            Sales_Team__c IN :salesTeamId
                        )
                    ])
                ).keySet();
    }

    public class SalesTeamServiceException extends Exception {}
}