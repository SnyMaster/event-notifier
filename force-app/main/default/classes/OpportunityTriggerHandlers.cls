/**
 * Created by Serg on 24.05.2021.
 */

public with sharing class OpportunityTriggerHandlers {

    public static void triggerHandler(List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpportunitiesMap, System.TriggerOperation triggerEvent) {

        SalesTeamEventHandler.triggerHandler(newOpportunities, oldOpportunitiesMap, triggerEvent);
    }
}